﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace prueba_CRUD
{
    class Cliente:Persona, ICrud
    {
        //variables BD
        //vars de connection string
        string us = "root";
        string pwd = "123456";
        string bd = "escuela";
        string server = "127.0.0.1";
        MySqlConnection con;
        MySqlCommand com;
        MySqlDataReader dr;

        //VARIABLES DE ERROS
        public static string errorMsg = "";
        //CONSTRUCTOR DE CLIENTE
        public Cliente(string nom, string apPA, string apMA, string fecha, string cel, string tel, string correo, Domicilio dom, string ineCompro, string curp, string curpCompro):
            base(nom, apPA, apMA, fecha, cel, tel, correo, dom, ineCompro, curp, curpCompro)
        {
            con = new MySqlConnection("Server=" + server + ";Database=" + bd + ";Uid=" + us + ";Pwd=" + pwd + ";");
        }
        public bool alta()
        {

            //DEFINIR EL RES BOOL
            bool res = false;


            //PROGRAMAR LA ACCION DEL BOTON
            try
            {

                abrirConexion();
                string query = "INSERT INTO clientes(nombre,apellidoP,apellidoM,fechaNacimiento,numCelular,telCasa,correo,domicilio,ineCompro,curp,curpCompro) " +
                               "VALUES ('" + this.nombre + "','" + this.apellidoP + "','" + this.apellidoM + "','" + this.fechaNacimiento + "','" + this.numnumCelular + "','" + this.telCasa + "','" + this.correo + "','" + this.Domicilio. + "','" + txtIne.Text + "','" + txtCurp.Text + "','" + txtCurpCompro.Text + "');";

                com = new MySqlCommand(query, con);
            //INSERT, DELETE, UPDATE
                com.ExecuteNonQuery();

                res = true;

            }
            catch (MySqlException mysqlex)
            {
                errorMsg = "ERROR SQL al insertar nuevo cliente. " + mysqlex.Message;
            }
            catch(Exception ex)
            {
                errorMsg = "ERROR general al insertar nuevo cliente. " + ex.Message;
            }
            finally
            {
                CerrarConexion();
            }
            //REGRASMOS
            return res;
        }
        public void CerrarConexion()
        {
            if (con.State !=System.Data.ConnectionState.Closed)
            {
                con.Close();
            }
        }
        public void abrirConexion()
        {
            if (con.State !=System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
        }
    public bool modificar(object datos, int id)
        {
            return  true;
        }
  public bool eliminar(int id)
        {
            return true;
        }
        List<object> Consultar(object criteriosBusqueda)
        {
            return true;
        }
      
    }
 
}
