﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba_CRUD
{
    class Domicilio
    {
        public string calle;
        public string numero;
        public string colonia;
        public string seccionFraccionamiento;
        public string codigoPostal;
        public string localidad;
        public string municipio;
        public string fotoComprobante;
    }
}
