﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba_CRUD
{
    interface ICrud
    {
        bool alta();
        bool modificar(object datos, int id);
        bool eliminar(int id);
        List<object> Consultar(object criteriosBusqueda);
    }
}
