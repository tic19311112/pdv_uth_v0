﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba_CRUD
{
    class Persona
    {
        protected string nombre;
        protected string apellidoP;
        protected string apellidoM;
        protected string fechaNacimiento;
        protected string numnumCelular;
        protected string telCasa;
        protected string correo;
        protected Domicilio domicilio;
        protected string ineCompro;
        protected string curp;
        protected string curpCompro;

        public string Nombre { get => nombre; set => nombre = value; }
        public string ApellidoP { get => apellidoP; set => apellidoP = value; }
        public string ApellidoM { get => apellidoM; set => apellidoM = value; }
        public string FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string NumnumCelular { get => numnumCelular; set => numnumCelular = value; }
        public string TelCasa { get => telCasa; set => telCasa = value; }
        public string Correo { get => correo; set => correo = value; }
        public Domicilio Domicilio { get => domicilio; set => domicilio = value; }
        public string IneCompro { get => ineCompro; set => ineCompro = value; }
        public string Curp { get => curp; set => curp = value; }
        public string CurpCompro { get => curpCompro; set => curpCompro = value; }

        public Persona(string nom, string apPA, string apMA, string fecha, string cel, string tel, string correo, Domicilio dom, string ineCompro, string curp, string curpCompro)
        {
            this.Nombre = nom;
            this.ApellidoP = apPA;
            this.ApellidoM = apMA;
            this.FechaNacimiento = fecha;
            this.NumnumCelular = cel;
            this.TelCasa = tel;
            this.correo = correo;
            this.Domicilio = dom;
            this.IneCompro = ineCompro;
            this.Curp = curp;
            this.CurpCompro = curpCompro;
        }
    }
}
