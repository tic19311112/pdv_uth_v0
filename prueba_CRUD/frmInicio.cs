﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace prueba_CRUD
{
    public partial class frmInicio : Form
    {
        //variables BD
        //vars de connection string
        string us = "root";
        string pwd = "123456";
        string bd = "escuela";
        string server = "127.0.0.1";
        MySqlConnection con;
        MySqlCommand com;
        MySqlDataReader dr;
        public frmInicio()
        {
            InitializeComponent();
            con = new MySqlConnection("Server=" + server + ";Database=" + bd + ";Uid=" + us + ";Pwd=" + pwd + ";");
        }

        private void FrmInicio_Load(object sender, EventArgs e)
        {
            limpiarform();
            try
            {
                abrirConexion();
                
                string query = "SELECT * FROM clientes";
                
                com = new MySqlCommand(query, con);
                
                dr = com.ExecuteReader();
               
                if (dr.HasRows)
                {
                   
                    while (dr.Read())
                    {
                         
                        int numCampos = dr.FieldCount;
                        
                        object[] nuevoRenglon = new object[numCampos];
                        
                        for (int i = 0; i < nuevoRenglon.Length; i++)
                        {
                            nuevoRenglon[i] = dr[i];
                        }
                        
                        dgClientes.Rows.Add(nuevoRenglon);
                    }
                   
                }
                else
                {
                    MessageBox.Show("No hay alguien registrado");
                }
            }
            catch (MySqlException mysqlex)
            {
                MessageBox.Show("Error de sintaxis SQL al consultar clientes" + mysqlex.Message, "Cargar datos del clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error de general SQL al consultar Cliente" + ex.Message, "Cargar datos de Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                
                if (dr != null)
                    dr.Close();
                CerrarConexion();
            }
        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }
        
        public void limpiarform()
        {
            //borramos
            txtNom.Text = "";
            txtAP.Text = "";
            txtAM.Text = "";
            txtCorreo.Text = "";
            txtNum.Text = "";
            txtFN.Text = "";
            txtDomicilio.Text = "";
            txtTel.Text = "";
            txtIne.Text = "";
            txtCurp.Text = "";
            txtCurpCompro.Text = "";
            //desabilitar todos los textbox
            txtNom.Enabled = false;
            txtAP.Enabled = false;
            txtAM.Enabled = false;
            txtCorreo.Enabled= false;
            txtNum.Enabled = false;
            txtFN.Enabled = false;
            txtDomicilio.Enabled = false;
            txtTel.Enabled = false;
            txtIne.Enabled = false;
            txtCurp.Enabled = false;
            txtCurpCompro.Enabled = false;

        }
        public void habilitar()
        {
            txtNom.Enabled = true;
            txtAP.Enabled = true;
            txtAM.Enabled = true;
            txtCorreo.Enabled = true;
            txtNum.Enabled = true;
            txtFN.Enabled = true;
            txtDomicilio.Enabled = true;
            txtTel.Enabled = true;
            txtIne.Enabled = true;
            txtCurp.Enabled = true;
            txtCurpCompro.Enabled = true;
        }
        public void CerrarConexion()
        {
            if (con.State != ConnectionState.Closed)
            {
                con.Close();
            }
        }
        public void abrirConexion()
        {
            if(con.State!=ConnectionState.Open)
            {
                con.Open();
            }
        }


    private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarform();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            habilitar();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea Salir?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
             
                this.Close();
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            //instancia del cliente
            Cliente cli = new Cliente(txtNom,);
            //ejecutar el metodo alta y recibir bool de res

            //vamosn a notificar el usurario con msgBox
            try
            {
                abrirConexion();

                string query = "INSERT INTO clientes(nombre,apellidoP,apellidoM,fechaNacimiento,numCelular,telCasa,correo,domicilio,ineCompro,curp,curpCompro) " +
                               "VALUES ('" + txtNom.Text + "','" + txtAP.Text + "','" + txtAM.Text + "','" + txtFN.Text + "'," + txtNum.Text + "','" + txtTel.Text + "','" + txtCorreo.Text + "','" + txtDomicilio.Text + "','" + txtIne.Text + "','" + txtCurp.Text + "','" + txtCurpCompro.Text + "');";
               
                com = new MySqlCommand(query, con);
                
                com.ExecuteNonQuery();
                
                MessageBox.Show("Cliente creado", "Nuevo Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                limpiarform();
                
                dgClientes.Rows.Clear();
                
                
                
            }
            catch (MySqlException mysqlex)
            {
                MessageBox.Show("Error SQL al crear cliente. " + mysqlex.Message, "Nuevo Cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error general al crear cliente. " + ex.Message, "Nuevo cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                CerrarConexion();
            }
        }

        private void BntEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Borrar cliente?", "Borrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question)==DialogResult.Yes)
                {
                    try
                    {
                        abrirConexion();
                        string query = "DELETE FROM clientes WHERE= " + id_clientes;
                        com = new MySqlCommand(query, con);
                        com.ExecuteNonQuery();
                        MessageBox.Show("Ya se borro al cliente", "Borrar Cliente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        limpiarform();
                        dgClientes.Rows.Clear();
                        FrmInicio_Load(this, new EventArgs());
                    }
                     catch (MySqlException mysqlex)
                    {
                    MessageBox.Show("Error al eliminar el cilente." + mysqlex.Message, "Eliminar cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } 
                       catch(Exception ex)
                    {
                    MessageBox.Show("Error al elimniar el cliente." + ex.Message, "Eliminar cliente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    }
                finally
                {
                    CerrarConexion();
                    
                }
            }
        }
    }
}

